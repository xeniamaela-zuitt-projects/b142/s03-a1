package com.abano.b142.s02.s02app.models;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {

    //Properties (columns)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //primary key
    @Column
    private String username;
    @Column
    private String password;

    //Constructors
    public User(){}
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    //Getters & Setters
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
